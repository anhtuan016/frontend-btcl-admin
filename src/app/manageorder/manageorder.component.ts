import { Component, OnInit } from '@angular/core';
import { OrderserviceService } from './orderservice.service';
import { ViewEncapsulation } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-manageorder',
  templateUrl: './manageorder.component.html',
  styleUrls: ['./manageorder.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ManageorderComponent implements OnInit {
  list: any[];
  limit: number = 10;
  p: number;
  total: number;
  detailObj = {};
  index: number;
  orderId: string;
  modalObj: any;
  boo_success: boolean = false;
  msg_success: string;
  boolean_search: boolean = false;
  keyword: string = '';
  pair_filter: string = '';
  type_filter: string = '';
  status_filter: string = '';
  valid_filter: string = '';
  param: string = '?';
  header: string = '';
  param_count: number = 0;
  constructor(private service: OrderserviceService, private modal: NgbModal) { }


  ngOnInit() {
    this.getPage(1);
  }
  search(keyword) {
    this.param_count = 0;
    this.keyword = keyword;
    this.getPage(1);
  }
  getPage(page: number) {
    if (this.keyword.length == 0 && this.param_count == 0) {
      this.boolean_search = false;
      this.service.getOrderPaginated(page, this.limit).subscribe(resp => {
        this.list = resp['data'];
        console.log('Current List: ' + JSON.stringify(this.list));
        this.p = resp['page'];
        this.total = resp['totalItem'];
      }, err => {
        console.log(err.error.detail);
      });
    } else if(this.keyword.length>0) {
      this.boolean_search = true;
      this.service.search(this.keyword, page, this.limit).subscribe(resp => {
        this.list = resp['data'];
        this.p = page;
        this.total = resp['totalItem'];
        console.log('Current List after search:' + JSON.stringify(this.list));
      }, err => {
        console.error(JSON.stringify(err.error));
      })
    }else{
      this.service.filter(this.header, this.param,page,this.limit).subscribe(resp => {
        this.list = resp['data'];
        this.total = resp['totalItem'];
        this.p = resp['page'];
        console.log(JSON.stringify(this.list));
      }, err => {
        console.error(err.error.detail);
      });

    }

  }
  getDetail(i, detail) {
    this.index = i;
    var id = this.list[this.index].id;
    this.service.getDetail(id).subscribe(resp => {
    if(this.modalObj!==null&&this.modalObj!==undefined) this.modalObj.close();
      this.detailObj = resp['data'];
    this.modalObj=  this.modal.open(detail);
      console.log("Detail Order: " + JSON.stringify(this.detailObj));
    }, err => {
      console.log(err.error.detail);
    });
  }
  doDelete(i, confirm) {
    this.index = i;
    this.orderId = this.list[this.index].id;
    this.modalObj = this.modal.open(confirm);
  }
  delete(confirm) {
    this.boo_success = false;
    this.modalObj.close();
    this.modalObj = this.modal.open(confirm);
    console.log('orderId: ' + this.orderId)
    this.service.delete(this.orderId).subscribe(resp => {
      this.boo_success = true;
      this.msg_success = 'Order is canceled successfully'
      this.getPage(this.p);
    }, err => {
      this.boo_success = true;
      this.msg_success = err.error.detail;
      console.log(err.error.detail);
    });
  }
  // FILTER FUNCTION........
  filter_pair(val) {
    this.pair_filter = val;
  }
  filter_type(val) {
    this.type_filter = val;
  }
  filter_status(val) {
    this.status_filter = val;
  }
  filter_valid(val) {
    this.valid_filter = val;
  }

  filter() {
    this.param_count=0;
    this.header = '';
    this.param = '?';
    this.keyword = '';
    if (this.pair_filter !== '') {
      this.param_count += 1;
      this.header += '-pair';
      this.param += ('pair=' + this.pair_filter + '&');

    }
    if (this.type_filter !== '') {
      this.param_count += 1;
      this.header += '-type';
      this.param += ('type=' + this.type_filter + '&');
    }
    if (this.status_filter !== '') {
      this.param_count += 1;
      this.header += '-status';
      this.param += ('status=' + this.status_filter + '&');
    }
    if (this.valid_filter !== '') {
      this.param_count += 1;
      this.header += '-valid';
      this.param += ('valid=' + this.valid_filter + '&');
    }
    this.param = this.param.substring(0, this.param.length - 1);
    console.log('header: ' + this.header);
    console.log('param: ' + this.param);
    console.log('key: '+this.keyword);
    this.getPage(1);
    


 


  }



}
