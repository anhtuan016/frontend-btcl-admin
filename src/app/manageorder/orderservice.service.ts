import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrderserviceService {
  url = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageorder/";
  url_delete = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageorder/cancelorder";
  url_search = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/search/searchorder";
  constructor(private httpclient: HttpClient) { }

  public getOrderPaginated(page, limit) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Authentication': token,
        'Content-type': 'application/json'
      }),
    };
    return this.httpclient.get(this.url + '?page=' + page + '&limit=' + limit, httpOptions);
  }
  public search(keyword, page, limit) {
    var token = window.localStorage.getItem("token");
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    return this.httpclient.get(this.url_search + '?k=' + keyword + '&page=' + page + '&limit=' + limit, httpOptions);
  }
  public getDetail(id) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Authentication': token,
        'Content-type': 'application/json'
      })
    };
    return this.httpclient.get(this.url + '?id=' + id, httpOptions);
  }
  public delete(id) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Authentication': token,
        'Content-type': 'application/json'
      })
    };
    return this.httpclient.delete(this.url_delete+'?id='+id, httpOptions);
  }

  public filter(header,param,page,limit){
    var token = window.localStorage.getItem("token");
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token,
        'filter':header
      })
    };
    return this.httpclient.get(this.url+param+'&page='+page+'&limit='+limit,httpOptions);
  }
}
