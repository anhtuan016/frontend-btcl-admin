import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ManageuserComponent } from './manageuser/manageuser.component';
import { ManageorderComponent } from './manageorder/manageorder.component';
import { BanneduserComponent } from './manageuser/banneduser.component';
import { ManagetransactionComponent } from './managetransaction/managetransaction.component';
import { ActivityhistoryComponent } from './activityhistory/activityhistory-admin.component';
import { ActivityhistoryUserComponent } from './activityhistory/activityhistory-user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CurrencyStatisticComponent } from './dashboard/currency-statistic/currency-statistic.component';


const routes: Routes = [
  {
    path: 'user', children: [
      { path: '', component: ManageuserComponent },
      { path: 'ban', component: BanneduserComponent }
    ]
  },
  { path: 'order', component: ManageorderComponent },
  { path: 'transaction', component: ManagetransactionComponent },
  {
    path: 'history', children: [
      { path: 'admin', component: ActivityhistoryComponent },
      { path: 'user', component: ActivityhistoryUserComponent },
      { path: "", redirectTo: 'admin', pathMatch: 'full' }
    ]
  },
  {path:'dashboard',children:[
    {path:'',component:DashboardComponent},
    {path:'wallets',component:CurrencyStatisticComponent}
  ]},
  { path: '**', redirectTo: '', pathMatch: 'full' }

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})

export class AppRoutingModule { }
