import { Component } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent {
  title = 'Admin Site';
  flag_icon_eng = '../assets/icon-eng.png';
  login = true;
  loginAlert = false;
  loginMsg = '';
  username = '';
  constructor(private service: AppService) { }
  ngOnInit(): void {
    var email = window.localStorage.getItem('email');
    if (email !== null && email !== undefined &&''!==email){
      this.username = email.split('@')[0];
    }
    // if((window.location.pathname).split("/")[1] === "dashboard") onload = handleClientLoad();
      this.goHomePage();
  }
  goHomePage(): void {
    var endpoint = window.location.pathname.split('/')[1];
    console.log('endpoint length: ' + endpoint.length);
    var tokenKey = window.localStorage.getItem("token");
    if (tokenKey === undefined || tokenKey === null) {
      this.login = true;
    }
    if ((tokenKey === undefined || tokenKey === null) && endpoint.length !== 0) {
      console.log('redirect')
      window.location.assign('');
    }
    if (tokenKey !== undefined && tokenKey !== null) {
      this.login = false;
    }

    if (tokenKey !== undefined && tokenKey !== null && endpoint.length === 0) {
      this.login = false;
      window.location.assign('/user'); //Change default redirect page here...
    }
  }
  doLogin(loginForm, email): void {
    console.log(email);
    this.service.login(loginForm.value).subscribe(data => {
      var token = data['tokenKey']
      console.log('token: ' + token);
      this.loginAlert = false;
      this.username = email;
      console.log('username: ' + this.username);
      window.localStorage.setItem('token', token);
      window.localStorage.setItem('email', email);
      window.location.assign('/user');
    }, err => {
      this.loginAlert = true;
      this.loginMsg = err.error.detail;
    });
  }
  doLogout(): void {
    window.localStorage.removeItem('token');
    window.location.assign("");
  }
}

