export function StartAnalytics(ACCESS_TOKEN) {
    // var ACCESS_TOKEN = "ya29.c.ElkUBs2lNVptkzTxcDl0yLxbEOt7ni7UIjoMRTWuEP9LVp6wo3qaXDuVoX_v6C5kdUQJMChpxSRoc_geJ18o8K1g8xWNJkn8S5ggPWN6yqWLEyndAnEfz_3zDQ";

    gapi.analytics.ready(function () {

        /**
         * Authorize the user immediately if the user has already granted access.
         * If no access has been created, render an authorize button inside the
         * element with the ID "embed-api-auth-container".
         */
        gapi.analytics.auth.authorize({
            serverAuth: {
                access_token: ACCESS_TOKEN
            }
        });


        /**
         * Create a new ActiveUsers instance to be rendered inside of an
         * element with the id "active-users-container" and poll for changes every
         * five seconds.`
         */
        var activeUsers = new gapi.analytics.ext.ActiveUsers({
            container: 'active-users-container',
            pollingInterval: 5
        });


        /**
         * Add CSS animation to visually show the when users come and go.
         */
        activeUsers.once('success', function () {
            var element = this.container.firstChild;
            var timeout;

            this.on('change', function (data) {
                var element = this.container.firstChild;
                var animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
                element.className += (' ' + animationClass);

                clearTimeout(timeout);
                timeout = setTimeout(function () {
                    element.className =
                        element.className.replace(/ is-(increasing|decreasing)/g, '');
                }, 3000);
            });
        });


        /**
         * Create a new ViewSelector2 instance to be rendered inside of an
         * element with the id "view-selector-container".
         */
        var viewSelector = new gapi.analytics.ext.ViewSelector2({
            container: 'view-selector-container',
            ids: '180053006'
        })
            .execute();


        /**
         * Update the activeUsers component, the Chartjs charts, and the dashboard
         * title whenever the user changes the view.
         */
        viewSelector.on('viewChange', function (data) {
            var title = document.getElementById('view-name');
            title.textContent = data.property.name + ' (' + data.view.name + ')';

            // Start tracking active users for this view.
            activeUsers.set(data).execute();

            renderTotalUsers(data.ids);
            renderNewSessions(data.ids);
            renderTotalVisitors(data.ids);
            renderBounceRate(data.ids);
        });

        // My data doing
        function renderTotalUsers(ids) {
            query({
                'ids': ids,
                'dimensions': 'ga:medium',
                'metrics': 'ga:users',
                'start-date': '2018-05-05',
                'end-date': 'today'
            }).then(function (response) {
                var current;
                if (response.hasOwnProperty("totalsForAllResults")) {
                    current = response["totalsForAllResults"];
                    current = current["ga:users"];
                    $('#totalUsersNum').html(current);
                }
                query({
                    'ids': ids,
                    'dimensions': 'ga:medium',
                    'metrics': 'ga:users',
                    'start-date': '2018-05-05',
                    'end-date': '30daysAgo'
                }).then(function (response) {
                    if (response.hasOwnProperty("totalsForAllResults")) {
                        var ago = response["totalsForAllResults"];
                        ago = ago["ga:users"];
                        var total = (ago / current) * 100;
                        if (total == 0 && current == 0) {
                            $('#percentageUsers').html('+' + Math.round(total) + '%');
                            // $('.totalUsers i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total == 0 && current > 0) {
                            $('#percentageUsers').html('+' + Math.round(current) * 100 + '%');
                            $('.totalUsers i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total > 0 && current > ago) {
                            $('#percentageUsers').html('+' + Math.round(total) + '%');
                            $('.totalUsers i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total > 0 && current < ago) {
                            total = total.slice(1);
                            $('#percentageUsers').html('-' + Math.round(total) + '%');
                            $('.totalUsers i').addClass('fa-rotate-90');
                        }
                    }
                });
            });
        }

        function renderNewSessions(ids) {
            query({
                'ids': ids,
                'dimensions': 'ga:medium',
                'metrics': 'ga:sessions',
                'start-date': '2018-05-05',
                'end-date': 'today'
            }).then(function (response) {
                var current;
                if (response.hasOwnProperty("totalsForAllResults")) {
                    var current = response["totalsForAllResults"];
                    current = current["ga:sessions"];
                    $('#totalSessionsNum').html(current);
                }
                query({
                    'ids': ids,
                    'dimensions': 'ga:medium',
                    'metrics': 'ga:sessions',
                    'start-date': '2018-05-05',
                    'end-date': '30daysAgo'
                }).then(function (response) {
                    if (response.hasOwnProperty("totalsForAllResults")) {
                        var ago = response["totalsForAllResults"];
                        ago = ago["ga:sessions"];
                        var total = (ago / current) * 100;
                        if (total == 0 && current == 0) {
                            $('#percentageSessions').html('+' + Math.round(total) + '%');
                            // $('.totalUsers i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total == 0 && current > 0) {
                            $('#percentageSessions').html('+' + Math.round(current) * 100 + '%');
                            $('.sessions i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total > 0 && current > ago) {
                            $('#percentageSessions').html('+' + Math.round(total) + '%');
                            $('.sessions i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total > 0 && current < ago) {
                            total = total.slice(1);
                            $('#percentageSessions').html('-' + Math.round(total) + '%');
                            $('.sessions i').addClass('fa-rotate-90');
                        }
                    }
                });
            });
        }

        function renderTotalVisitors(ids) {
            query({
                'ids': ids,
                'dimensions': 'ga:medium',
                'metrics': 'ga:pageviews',
                'start-date': '2018-05-05',
                'end-date': 'today'
            }).then(function (response) {
                var current;
                if (response.hasOwnProperty("totalsForAllResults")) {
                    current = response["totalsForAllResults"];
                    current = current["ga:pageviews"];
                    $('#totalVisitorsNum').html(current);
                }
                query({
                    'ids': ids,
                    'dimensions': 'ga:medium',
                    'metrics': 'ga:pageviews',
                    'start-date': '2018-05-05',
                    'end-date': '30daysAgo'
                }).then(function (response) {
                    var ago;
                    if (response.hasOwnProperty("totalsForAllResults")) {
                        var ago = response["totalsForAllResults"];
                        ago = ago["ga:pageviews"];
                        var total = (ago / current) * 100;
                        if (total == 0 && current == 0) {
                            $('#percentageVisitors').html('+' + Math.round(total) + '%');
                            $('.visitors i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total == 0 && current > 0) {
                            $('#percentageVisitors').html('+' + Math.round(current) * 100 + '%');
                            $('.visitors i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total > 0 && current > ago) {
                            $('#percentageVisitors').html('+' + Math.round(total) + '%');
                            $('.visitors i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total > 0 && current < ago) {
                            total = total.slice(1);
                            $('#percentageVisitors').html('-' + Math.round(total) + '%');
                            $('.visitors i').addClass('fa-rotate-90');
                        }
                    }
                });
            });
        }


        function renderBounceRate(ids) {
            query({
                'ids': ids,
                'dimensions': 'ga:medium',
                'metrics': 'ga:bounceRate',
                'start-date': '2018-05-05',
                'end-date': 'today'
            }).then(function (response) {
                var current;
                if (response.hasOwnProperty("totalsForAllResults")) {
                    current = response["totalsForAllResults"];
                    current = current["ga:bounceRate"];
                    $('#totalBounceRateNum').html(Math.round(current) + "%");
                }
                query({
                    'ids': ids,
                    'dimensions': 'ga:medium',
                    'metrics': 'ga:bounceRate',
                    'start-date': '2018-05-05',
                    'end-date': '30daysAgo'
                }).then(function (response) {
                    var ago;
                    if (response.hasOwnProperty("totalsForAllResults")) {
                        var ago = response["totalsForAllResults"];
                        ago = ago["ga:bounceRate"];
                        var total = current - ago;
                        if (total == 0 && current == 0) {
                            $('#percentageBounceRate').html('+' + Math.round(total) + '%');
                            $('.rate i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total <= 0 && current > 0) {
                            $('#percentageBounceRate').html('+' + Math.round(current) + '%');
                            $('.rate i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total > 0 && current > ago) {
                            $('#percentageBounceRate').html('+' + Math.round(total) + '%');
                            $('.rate i').addClass('fa-rotate-270 text-warning');
                        }
                        if (total > 0 && current < ago) {
                            $('#percentageBounceRate').html(Math.round(total) + '%');
                            $('.rate i').addClass('fa-rotate-90');
                        }
                    }
                });
            });
        }


        /**
         * Extend the Embed APIs `gapi.analytics.report.Data` component to
         * return a promise the is fulfilled with the value returned by the API.
         * @param {Object} params The request parameters.
         * @return {Promise} A promise.
         */
        function query(params) {
            return new Promise(function (resolve, reject) {
                var data = new gapi.analytics.report.Data({ query: params });
                data.once('success', function (response) {
                    resolve(response);
                })
                    .once('error', function (response) {
                        reject(response);
                    })
                    .execute();
            });
        }


        /**
         * Create a new canvas inside the specified element. Set it to be the width
         * and height of its container.
         * @param {string} id The id attribute of the element to host the canvas.
         * @return {RenderingContext} The 2D canvas context.
         */
        function makeCanvas(id) {
            var container = document.getElementById(id);
            var canvas = document.createElement('canvas');
            var ctx = canvas.getContext('2d');

            container.innerHTML = '';
            canvas.width = container.offsetWidth;
            canvas.height = container.offsetHeight;
            container.appendChild(canvas);

            return ctx;
        }


        /**
         * Create a visual legend inside the specified element based off of a
         * Chart.js dataset.
         * @param {string} id The id attribute of the element to host the legend.
         * @param {Array.<Object>} items A list of labels and colors for the legend.
         */
        function generateLegend(id, items) {
            var legend = document.getElementById(id);
            legend.innerHTML = items.map(function (item) {
                var color = item.color || item.fillColor;
                var label = item.label;
                return '<li><i style="background:' + color + '"></i>' +
                    escapeHtml(label) + '</li>';
            }).join('');
        }


        // Set some global Chart.js defaults.
        Chart.defaults.global.animationSteps = 60;
        Chart.defaults.global.animationEasing = 'easeInOutQuart';
        Chart.defaults.global.responsive = true;
        Chart.defaults.global.maintainAspectRatio = false;


        /**
         * Escapes a potentially unsafe HTML string.
         * @param {string} str An string that may contain HTML entities.
         * @return {string} The HTML-escaped string.
         */
        function escapeHtml(str) {
            var div = document.createElement('div');
            div.appendChild(document.createTextNode(str));
            return div.innerHTML;
        }

    });

}



function getCert() {
    var cert = //your json key (downloaded earlier) goes here
    {
        "type": "service_account",
        "project_id": "bittrexcln",
        "private_key_id": "fd998ca8845303b8cff118e35606e74165fa6987",
        "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC/tjeIxH1wqoz+\nLvGykL4D7CQ7gzPrk5vMgSdHoTebRyb3GJkS6GbCc5IfTTC18NVEz/wsGekQHrbb\n4r1CuTggI1wf1JnoGXVYfCorSV1ffeNclxGsFV2EWVoZAJaMPEVy3n7vm1jhS2mN\nVxr7x3Irmh3VOMq8QoxPHwVhuLoM1Dw0ODYgW5c6jCSeF1vaAPCNPeDHoEoZu8Iu\nme3IqcI98lnO5451R5UXQLZQ9I4V5L0ndVxXg3Lg7ZaUycE/jgNkFagB/YcB/C6B\ndCwuTPxtim6XrP82awAu7BS45PJ59h/R5QeJSF9SID3T3D1FRvAQ4peSNgqbLynM\ndn6MWAhJAgMBAAECggEAEIkiQZ73FeJ0DlGUwdrntNVnq6Vts0ZUVB+YuH4wmmlY\n9EuwGlgm0uNeiWoiRzMqZgjNfq1Oc7g+e/lS60AoFeeO4OT40HLvsu3kwj6N+fhV\nBc229cGTAyEk2w64rYheUiHcT4T19drIFCuc9XYWxCmDJLrFC9dunD13/J2PGJ+I\niGwRCLO7rjObjp9Ff0Lc7KTKt/i5XvwB1ZPUyccMvgfImNFUhIXKllVtVQNAGoQB\nvxuyI3jdgjjcrM/okuxH/tA5UF1u4mLJg/NMfVsVIXoLIHIfxFX2GSuKBRj1RL3E\nzjB7frBx0i6fias19Y/unzLcU+TgkWYDHEap7LoCzQKBgQDglWWdlPO9v0at4Ezq\nAl4oWq/I3GQrtLdS4AHiUuFJtHpynS9y2+z+dpo5Bf2X5I0PBHZUlK68LSfEGVxv\nC2198XSHOeeXlv+74wR4mzesQ9bES7gZtXWvE1uHU1mJaW2gLTtgDct0heazbdO9\nlz19IfM//kVzFmHnzT+A9xP3TQKBgQDah6RkofgvzYX8sSQBzJHBu3vh+CT491jt\nXIj4VRqiFbY8YDLve/K6N88Glc9osMy5yCEKoRSAlFlUMIR0+xrl4jVWa7ueypeN\nHN2G3lwE50SpECd5JLg0Pf0Hx8wWWVH1cyCpdLhePd2NJdxQl0YAZ/p4aUflncBt\n/gCUYn1u7QKBgQDFd5bZb9bAyCZFTCtUtrBDmzlkyyPoqgVnAT+m2GjufZoMnNjT\njTxcobwI15OasoK06Z+Bzxm0q5+DdSkpGkszqdJj9C4Fw+coNPwBb7kL5ro37rv5\nHuxS/BpGaqsXf3O8mwjR9E8oLXQj4DWFRi3ykJ9pbRFzmAKN5pInuxf7IQKBgFtW\nVl+5UwKn5Fi5sVMPLdzLf/vyu4FT4NdU1er1pywX5xNVIfT22Xz71ovqEcawHuGH\nxEetC+SyHKQTo0lBqvxqj+CPJtw30SAp8G1ugweJoikHl3gK4PL09c1KtQKiUp9E\nf2JUZrr9IJzeNjd5AskorIeneWV/ie5vO87JW6QBAoGBALAJYIbPXg9VDXmJ0FGe\nBMCXN0yu+BqOPATAORtSiB8tdVCqPyVPKOt7fQSbI7oGFFRFAXIW39m/QQLoqgI9\nOm4wlqKrkIdeFqLyRelaGdpldRD9pmppcWqbeqf0/XYuhT6yyjJcnaJxo8qCBejK\nLtZre6Hjmc2r9EfCwRwcmt30\n-----END PRIVATE KEY-----\n",
        "client_email": "bittrexcln@appspot.gserviceaccount.com",
        "client_id": "",
        "auth_uri": "https://accounts.google.com/o/oauth2/auth",
        "token_uri": "https://oauth2.googleapis.com/token",
        "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
        "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/bittrexcln%40appspot.gserviceaccount.com"
      };
    return cert;
}
export function getJWT() {
    var cert = getCert();
    var key = KEYUTIL.getKey(cert.private_key);
    var headers = { "alg": "RS256", "typ": "JWT" };
    var issued = Math.floor(new Date().getTime() / 1000);

    var claims = {
        "iss": cert.client_email,
        "scope": "https://www.googleapis.com/auth/analytics https://www.googleapis.com/auth/analytics.edit https://www.googleapis.com/auth/analytics.readonly",
        "aud": "https://www.googleapis.com/oauth2/v4/token",
        "exp": issued + 3600,
        "iat": issued
    };

    var jwt = KJUR.jws.JWS.sign(headers.alg, headers, JSON.stringify(claims), key);
    return jwt;
}

// function postJWT(jwt) {
//     var xhttp = new XMLHttpRequest();
//     var parameters = "grant_type=" + encodeURIComponent("urn:ietf:params:oauth:grant-type:jwt-bearer") + "&assertion=" + encodeURIComponent(jwt);
//     xhttp.open("POST", "https://www.googleapis.com/oauth2/v4/token", true);
//     xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//     xhttp.send(parameters);
//     xhttp.onreadystatechange = function () {
//         if (this.readyState == 4) {
//             if (this.status == 200) {
//                 // console.log(this.responseText);
//                 return this.responseText;
//             }
//             // if (console) return this.responseText;
//         }
//     };
// }


// export function start() {
//     setTimeout(function () {
//         handleClientLoad();
//     }, 1000);
// }