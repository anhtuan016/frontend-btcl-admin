import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagetransactionComponent } from './managetransaction.component';

describe('ManagetransactionComponent', () => {
  let component: ManagetransactionComponent;
  let fixture: ComponentFixture<ManagetransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagetransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagetransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
