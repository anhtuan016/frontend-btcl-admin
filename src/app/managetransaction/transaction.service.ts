import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  url = 'https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/transaction';
 //?start&stop

  constructor(private httpClient: HttpClient) { }



  getPaginated(page, limit) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Authentication': token,
        'Content-type': 'application/json'
      })
    }
    return this.httpClient.get(this.url + '?page=' + page + '&limit=' + limit, httpOptions);
  }
  getFilterRange(start, stop, page, limit) {
    var token = window.localStorage.getItem('token');
    console.log('token: ' + token)
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token

      })
    }
    console.log(this.url + '?start=' + start + '&stop=' + stop + '&page=' + page + '&limit=' + limit);
    return this.httpClient.get(this.url + '?start=' + start + '&stop=' + stop + '&page=' + page + '&limit=' + limit,httpOptions);
  }
  getByPair(pair,page,limit){
    var token = window.localStorage.getItem('token');
    console.log('token: ' + token)
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token

      })
    }
    console.log('URL: '+this.url+'?pair='+pair+'&page='+page+'&limit='+limit)
    return this.httpClient.get(this.url+'?pair='+pair+'&page='+page+'&limit='+limit,httpOptions);
  }
}
              