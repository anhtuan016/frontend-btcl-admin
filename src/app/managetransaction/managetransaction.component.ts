import { Component, OnInit } from '@angular/core';
import { TransactionService } from './transaction.service';
import { } from '@angular/'

@Component({
  selector: 'app-managetransaction',
  templateUrl: './managetransaction.component.html',
  styleUrls: ['./managetransaction.component.css']
})
export class ManagetransactionComponent implements OnInit {
  limit: number = 10;
  p: number;
  total: number;
  list: any[];
  start: any = {};
  stop: any = {};
  boolean_filterRange: boolean;
  boolean_filterPair:boolean;
  constructor(private service: TransactionService) { }
  start_millisec: number;
  stop_millisec: number;
  pair:string;
  ngOnInit() {
    this.getPage(1);
  }
  getPage(page) {
    if (!this.boolean_filterRange&&!this.boolean_filterPair) {
      this.service.getPaginated(page, this.limit).subscribe(resp => {
        this.p = page;
        this.total = resp['totalItem'];
        this.list = resp['data'];
        console.log('data: ' + JSON.stringify(resp));
      }, err => {
        console.error(err.error.detail);
      })
    }
    if (this.boolean_filterRange) {
      this.service.getFilterRange(this.start_millisec, this.stop_millisec, page, this.limit).subscribe(resp => {
        console.log('resp: ' + JSON.stringify(resp));
        this.list = resp['data'];
        this.p = resp['page'];
        this.total = resp['totalItem'];
      }, err => {
        console.log(err.error.detail);
      });
    }
    if(this.boolean_filterPair){
      this.service.getByPair(this.pair,page,this.limit).subscribe(resp=>{
        console.log('resp: '+JSON.stringify(resp));
        this.list = resp['data'];
        this.p = resp['page'];
        this.total = resp['totalItem'];
      },err=>{
        console.error(err.error.detail);
      });
    }
  }
  filterRange() {
    this.boolean_filterRange = true;
    this.boolean_filterPair = false;
    console.log('start= ' + JSON.stringify(this.start));
    console.log('stop=' + JSON.stringify(this.stop));
    var startDate = new Date(Date.UTC(this.start.year, this.start.month-1, this.start.day,0,1,0));
    var stopDate = new Date(Date.UTC(this.stop.year, this.stop.month-1, this.stop.day,23,59,59));
    console.log('startDate='+JSON.stringify(startDate));
    console.log('stopDate='+JSON.stringify(stopDate));
    this.start_millisec = startDate.getTime();
    this.stop_millisec = stopDate.getTime();
    this.boolean_filterRange = true;
    this.getPage(1);
  }
  refreshFilter(){
    this.boolean_filterRange = false;
    this.boolean_filterPair = false;
    this.start={};
    this.stop={};
    this.getPage(1);
  }
  filter_pair(value){
    this.boolean_filterRange = false;
    this.boolean_filterPair = true;
    this.pair = value;
    this.getPage(1);
  }
  
}
