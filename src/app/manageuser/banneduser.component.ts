import { Component, OnInit } from '@angular/core';
import { BanneduserService } from './banneduser.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import {ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'app-banneduser',
  templateUrl: './banneduser.component.html',
  styleUrls: ['./banneduser.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BanneduserComponent implements OnInit {
  userPaginatedList: any[];
  p: number = 1;
  total: number;
  keyword: string = '';
  limit: number = 10;
  emailToBeReActive: string;
  modalObj: any;
  index: number;
  boo_success: boolean = false;
  msg_success: string;
  constructor(private service: BanneduserService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getPage(1);
  }

  getPage(page: number) {
    this.service.getUserPaginated(page, this.limit).subscribe(resp => {
      console.log(JSON.stringify(resp));
      this.userPaginatedList = resp['data'];
      this.p = page;
      this.total = resp['totalItem'];

    }, err => {
      console.log(JSON.stringify(err))
    })
  }
  confirm(confirmDelete, i) {
    this.index = i;
    this.emailToBeReActive = this.userPaginatedList[this.index].email;
    this.modalObj = this.modalService.open(confirmDelete);
  }
  activate(waitingModal) {
    this.modalObj.close();
    this.modalObj = this.modalService.open(waitingModal);
    this.boo_success = false;
    console.log(this.index);
    var reactivated_user = this.userPaginatedList[this.index];
    var obj = {
      email : reactivated_user.email,
      status : '1'
    }
    console.log(JSON.stringify(obj));
    this.service.updateUser(obj).subscribe(resp => {
      this.boo_success = true;
      this.msg_success = "Success";
      console.log('resp: ' + resp);
      this.getPage(this.p);
    }, err => {
      this.boo_success = true;
      this.msg_success = err.error.detail;
      console.log(err.error);
    })
  }
}
