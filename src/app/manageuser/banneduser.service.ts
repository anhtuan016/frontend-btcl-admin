import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class BanneduserService {
  url = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageuserdelete";
  url_updateUser = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageuser/edituser";
  constructor(private httpClient: HttpClient) { }

  public getUserPaginated(page, limit) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    };
    return this.httpClient.get(this.url + '?page=' + page + '&limit=' + limit, httpOptions);
  }
  public updateUser(updateForm) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    console.log('UpdateForm: ' + updateForm);
    return this.httpClient.put(this.url_updateUser, updateForm, httpOptions);
  }
}
