import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ManageuserService {
  url = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageuser";
  url_updateUser = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageuser/edituser";
  url_deleteuser = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageuser/deleteuser?id=";
  url_addUser = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageuser/registeruser";
  url_checkEmail = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/manageuser/checkemail";
  url_searchUser = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/search/searchuser";
  constructor(private httpClient: HttpClient) {

  }
  public getDetail(id){
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    };
    return this.httpClient.get(this.url+'?id='+id,httpOptions);
  }
    public getUser() {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    return this.httpClient.get(this.url, httpOptions);
  }
  public getUserPaginated(page, limit) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    };
    return this.httpClient.get(this.url + '?page=' + page + '&limit=' + limit, httpOptions);
  }
  public updateUser(updateForm) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    console.log('UpdateForm: ' + updateForm);
    return this.httpClient.put(this.url_updateUser, updateForm, httpOptions);
  }
  public addUser(registerForm) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    console.log("Register Form: " + registerForm);
    return this.httpClient.post(this.url_addUser, registerForm, httpOptions);
  }
  public deleteUser(id) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    console.log('user deleted: ' + id);
    return this.httpClient.delete(this.url_deleteuser + id, httpOptions);
  }
  public checkMail(email) {
    var token = window.localStorage.getItem('token');
    var body = {
      'email': email
    };
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    console.log('body: ' + JSON.stringify(body))
    return this.httpClient.post(this.url_checkEmail, body, httpOptions);
  }
  public search(keyword, page, limit) {
    var token = window.localStorage.getItem("token");
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    return this.httpClient.get(this.url_searchUser + '?k=' + keyword + '&page=' + page + '&limit=' + limit, httpOptions);
  }

}
