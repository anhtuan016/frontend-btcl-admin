import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';
import { ManageuserService } from './manageuser.service';
import { DatePipe } from '@angular/common';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';
import { AbstractControl, ValidatorFn } from '@angular/forms';

@Component({
  selector: 'app-manageuser',
  templateUrl: './manageuser.component.html',
  styleUrls: ['./manageuser.component.css'],
  providers: [
    ManageuserService,
    DatePipe
  ],
  encapsulation: ViewEncapsulation.None

})
export class ManageuserComponent implements OnInit {
  closeResult: string;
  index: number;
  editBtn = "Edit";
  revealUpdateBtn = true;
  disableUpdateBtn = false;
  modalObj: any;
  updateAlert = false;
  updateMsg: string;
  addAlert = false;
  validMail: string;
  emailTobeDeleted: string;
  addUserMsg: string;
  boo_validForm = false;
  boo_success: boolean;
  msg_success: string;
  regex_email = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
  regex_password = '^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,16}$';
  regex_name = '^[A-Za-z]{1,30}$';
  regex_country = '^[A-Za-z]{1,16}$'
  radio_box_true = true;
  radio_box_false = false;
  validate_msgwarning = "Example: example123@mail.example.com";
  userPaginatedList: any[];
  p: number = 1;
  total: number;
  keyword: string = '';
  limit: number = 10;
  boolean_search: boolean = false;
  detail_User= {};


  constructor(private service: ManageuserService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getPage(1);
  }


  openDetail(content, i) {
    this.index = i;
    this.service.getDetail(this.userPaginatedList[i].email).subscribe(resp => {
      if(this.modalObj!==null&&this.modalObj!==undefined) this.modalObj.close();
      this.detail_User = resp['data'];
      this.modalObj = this.modalService.open(content);      
      console.log('Detail_user: '+JSON.stringify(this.detail_User));
    }, err => {
      console.log(err.error.detail);
    })
    
  }
  closeDetail() {
    console.log('Nut edit:' + this.editBtn)
    if (this.editBtn === "Update") {
      console.log('vao day');
      this.editBtn = "Edit";
      console.log(this.editBtn);
    } else {
      console.log('Nut edit2:' + this.editBtn);
      this.modalObj.close();
    }
    console.log('Nut Edit 3: ' + this.editBtn)
  }
  addUser(content) {
    this.modalObj = this.modalService.open(content);
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: $(reason)`;
    }
  }
  doUpdate(updateForm, waitingModal) {
    console.log(updateForm.value);
    updateForm.value.status == true ? updateForm.value.status = '1' : updateForm.value.status = '2';
    console.log(updateForm.value);
    if (this.editBtn === "Update") {
      console.log(updateForm.value);
      this.service.updateUser(updateForm.value).subscribe(data => {
        console.log(updateForm.value)
        console.log(data);
        this.editBtn = "Edit";
        this.getPage(this.p)
        this.modalObj.close();
      }, err => {
        console.log(err.error);
        this.updateAlert = true;
        this.updateMsg = err.error;
      })
      return;
    }
    if (this.editBtn === "Edit") this.editBtn = "Update";
  }

  checkMailAvailable(email) {
    this.service.checkMail(email).subscribe(resp => {
      console.log('resp: ' + resp['result']);
      if ('true' == resp['result']) {
        this.boo_validForm = true;
        this.validate_msgwarning = "Example: example123@mail.example.com";
      }
      if ('false' == resp['result']) {
        this.boo_validForm = false;
        this.validate_msgwarning = "Email is dupplicated. Please choose another one!"
      }
    }, err => {
      console.log(err.error.result);
    })
    console.log(this.boo_validForm)
  }
  doAddUser(addUserForm) {
    console.log(addUserForm.value);
    this.service.addUser(addUserForm.value).subscribe(resp => {
      this.addAlert = false;
      console.log('success');
      this.modalObj.close();
      this.getPage(this.p);
    }, err => {
      console.log(err.error);
      this.addUserMsg = err.error.detail;
      this.addAlert = true;
    })
    this.validate_msgwarning = "Example: example123@mail.example.com";
  }
  doDelete(i, confirmDelete) {
    this.index = i;
    this.emailTobeDeleted = this.userPaginatedList[this.index].email;
    this.modalObj = this.modalService.open(confirmDelete);
  }
  delete(waitingModal) {
    this.modalObj.close();
    this.modalObj = this.modalService.open(waitingModal);
    this.boo_success = false;
    console.log(this.index);
    var id = this.userPaginatedList[this.index].email;
    console.log(id);
    this.service.deleteUser(id).subscribe(resp => {
      this.boo_success = true;
      this.msg_success = "Success";
      console.log('resp: ' + resp);
      this.getPage(this.p);
    }, err => {
      this.boo_success = true;
      this.msg_success = err.error.detail;
      console.log(err.error);
    })
  }


  // PAGINATION.

  getPage(page: number) {
    if (this.keyword.length == 0 || this.keyword === null || this.keyword === undefined) {
      this.boolean_search = false;
      this.service.getUserPaginated(page, this.limit).subscribe(resp => {
        console.log(JSON.stringify(resp));
        this.userPaginatedList = resp['data'];
        this.p = page;
        this.total = resp['totalItem'];

      }, err => {
        console.log(JSON.stringify(err))
      })
    } else {
      this.boolean_search = true;
      this.service.search(this.keyword, page, this.limit).subscribe(resp => {
        console.log(JSON.stringify(resp));
        this.userPaginatedList = resp['data'];
        this.p = page;
        this.total = resp['totalItem'];
      }, err => {
        console.log(JSON.stringify(err));
      });
    }


  }

  // SEARCH FUNCTION
  search(keyword) {
    this.keyword = keyword;
    this.getPage(1);
  }

}


