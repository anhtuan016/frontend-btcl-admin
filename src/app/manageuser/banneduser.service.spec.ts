import { TestBed, inject } from '@angular/core/testing';

import { BanneduserService } from './banneduser.service';

describe('BanneduserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BanneduserService]
    });
  });

  it('should be created', inject([BanneduserService], (service: BanneduserService) => {
    expect(service).toBeTruthy();
  }));
});
