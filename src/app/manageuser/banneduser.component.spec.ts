import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BanneduserComponent } from './banneduser.component';

describe('BanneduserComponent', () => {
  let component: BanneduserComponent;
  let fixture: ComponentFixture<BanneduserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BanneduserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BanneduserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
