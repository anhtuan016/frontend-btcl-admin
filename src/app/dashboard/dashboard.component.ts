import { Component, OnInit } from '@angular/core';
import { StartAnalytics, getJWT } from '../customer';
import { CookieService } from 'ngx-cookie-service';
import { AppService } from '../app.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  data; jwt; access_token;
  constructor(private cookieService: CookieService, private service: AppService) { }

  ngOnInit() {
    // this.cookieService.delete('access_token');
    this.access_token = this.cookieService.get('access_token');
    if (this.access_token === null || this.access_token === "" || this.access_token === undefined) {
      this.jwt = getJWT();
      if (this.jwt) this.GetAccessToken(this.jwt);
      return;
    }
    this.GetExprire();
    StartAnalytics(this.access_token);
  }
  GetAccessToken(jwt) {
    this.service.get_access_token_analytics(jwt)
      .subscribe(
        data => { this.data = data; this.cookieService.set('access_token', this.data.access_token); StartAnalytics(this.data.access_token) }
        , error => console.log(error)
      );
  }
  GetExprire() {
    this.service.test_exprire_token(this.access_token)
      .subscribe(
        data => {
          this.data = data;
          if (this.data.expires_in < 100) {
            this.jwt = getJWT();
            if (this.jwt) this.GetAccessToken(this.jwt);
            return;
          }
        }
        , error => {
          this.data = error;
          if (this.data.error.error == "invalid_token") {
            this.jwt = getJWT();
            if (this.jwt) this.GetAccessToken(this.jwt);
            return;
          }
        }
      );
  }
}
