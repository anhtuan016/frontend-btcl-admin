import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core';



import { CurrencyServiceService } from './currency-service.service';



const reducer = (accumulator, currentValue) => accumulator + currentValue;
@Component({
  selector: 'app-currency-statistic',
  templateUrl: './currency-statistic.component.html',
  styleUrls: ['./currency-statistic.component.css'],
  encapsulation: ViewEncapsulation.None
})


export class CurrencyStatisticComponent implements OnInit {
  pair: string = "BTC-ETH";
  labels = ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00'
    , '20:00', '21:00', '22:00', '23:00', '24:00'];
  data_dewi1: any = {};
  data_dewi2: any = {};
  data_tx: any = {};
  data_order: any = {};
  txCount: number = 0;
  orderCount: number = 10;
  coin_name1: string;
  coin_name2: string;
  time: any = {};
  time_milli: number;
  bal_1: number = 0;
  bal_2: number = 0;
  options = {
    animation: {
      duration: 0
    },
    hover: {
      animationDuration: 0
    },
    responsiveAnimationDuration: 0
  };
  constructor(private service: CurrencyServiceService) { }

  ngOnInit() {
    console.log(this.time);
    this.init_date();
    this.init_dewi();
    this.init_tx();
    this.init_order();
    this.init_balance();
  }
  init_dewi() {
    console.log(this.time_milli);
    // Initialize Table Deposit/Withdrawal Coin 1.
    this.service.getDeWiData(this.coin_name1, this.time_milli).subscribe(resp => {
      console.log(JSON.stringify(resp));
      this.data_dewi1 = new data(this.labels, resp);
    }, err => {
      console.log(err.error.detail);
    });
    // Initialize Table Deposit/Withdrawal Coin 2.
    this.service.getDeWiData(this.coin_name2, this.time_milli).subscribe(resp => {
      console.log(JSON.stringify(resp));
      this.data_dewi2 = new data(this.labels, resp);
    }, err => {
      console.log(err.error.detail);
    });
  };

  init_tx() {
    this.service.getTx(this.pair, this.time_milli).subscribe(resp => {
      console.log('Tx: ' + JSON.stringify(resp[0]['data']));
      if (resp[0]['data'].length != 0) {

      } else {
        this.txCount = 0;
      }
      console.log("Sum: " + this.txCount);

      this.data_tx = new data(this.labels, resp);
    }, err => {
      console.log(err.error.detail);
    })
  }
  init_order() {
    let sell_count: number = 0;
    let buy_count: number = 0;
    this.service.getOrder(this.pair, this.time_milli).subscribe(resp => {
      console.log(JSON.stringify(resp));
      console.log('data_sell: ' + JSON.stringify(resp[0]['data']));
      console.log('data_buy: ' + JSON.stringify(resp[1]['data']));
      if (resp[0]['data'].length != 0) {
        sell_count = resp[0]['data'].reduce(reducer);
      }
      if (resp[1]['data'].length != 0) {
        buy_count = resp[1]['data'].reduce(reducer);
      }
      this.orderCount = sell_count + buy_count;
      console.log('Order_Count: ' + this.orderCount);

      this.data_order = new data(this.labels, resp);
    }, err => {
      console.log(err.error.detail);
    })
  }
  pickpair(value) {
    console.log(value);
    this.pair = value;
    console.log(this.pair);
    this.coin_name1 = this.pair.split('-')[0];
    this.coin_name2 = this.pair.split('-')[1];
    this.init_dewi();
    this.init_tx();
    this.init_order();
    this.init_balance();
  }
  refreshFilter() {
    this.time = {};
    this.init_date();
    this.init_dewi();
    this.init_tx();
    this.init_order();
    this.init_balance();

    console.log(JSON.stringify(this.time));
  }
  goDate() {
    var date = new Date(Date.UTC(this.time.year, this.time.month - 1, this.time.day, 0, 1, 0));
    this.time_milli = date.getTime();
    this.init_dewi();
    this.init_tx();
    this.init_order();
    this.init_balance();
  }

  init_date() {
    var date = new Date();
    date.setUTCHours(0);
    date.setUTCMinutes(0);
    date.setUTCSeconds(0);
    this.time_milli = date.getTime();
    console.log('Init Time: ' + this.time_milli);
    this.coin_name1 = this.pair.split('-')[0];
    this.coin_name2 = this.pair.split('-')[1];

  }
  init_balance() {
    // Balance of Coin1
    this.service.getBalance(this.coin_name1).subscribe(resp => {
      console.log('Bal coin1 :' + resp);
      this.bal_1 = resp as number;
    }, err => {
      console.log(err as string);
    })
    // Balance of Coin2
    this.service.getBalance(this.coin_name2).subscribe(resp => {
      console.log('Bal coin2 :' + resp);
      this.bal_2 = resp as number;
    }, err => {
      console.log(err as string);
    })
  }
}
class data {
  labels: any = [];
  datasets: any = [];
  constructor(labels, datasets) {
    this.labels = labels;
    this.datasets = datasets;
  }
}
