import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CurrencyServiceService {
  url_dewi = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/getdatacharts/depowith";
  url_order = 'https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/getdatacharts/order';
  url_transaction = 'https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/getdatacharts/trans';
  url_balance = 'https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/getbalance?coin=';
  constructor(private httpClient: HttpClient) { }

  getDeWiData(coin, time) {
    var token = window.localStorage.getItem("token");
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    };
    console.log('URL: ' + this.url_dewi + '?coin=' + coin + '&time=' + time);
    return this.httpClient.get(this.url_dewi + '?coin=' + coin + '&time=' + time, httpOptions);
  }
  getTx(pair, time) {
    var token = window.localStorage.getItem("token");
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    };
    console.log('Tx URL: ' + this.url_transaction + '?pair=' + pair + '&time=' + time);
    return this.httpClient.get(this.url_transaction + '?pair=' + pair + '&time=' + time, httpOptions);
  }
  getOrder(pair, time) {
    var token = window.localStorage.getItem("token");
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    };
    console.log('Order URL: ' + this.url_order + '?pair=' + pair + '&time=' + time);
    return this.httpClient.get(this.url_order + '?pair=' + pair + '&time=' + time, httpOptions);
  }
  getBalance(coin) {
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json',
        'Authentication': token
      })
    }
    console.log('Balance URL: ' + this.url_balance + coin);
    return this.httpClient.get(this.url_balance + coin, httpOptions);
  }
}
