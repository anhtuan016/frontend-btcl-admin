import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyStatisticComponent } from './currency-statistic.component';

describe('CurrencyStatisticComponent', () => {
  let component: CurrencyStatisticComponent;
  let fixture: ComponentFixture<CurrencyStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrencyStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
