import { TestBed, inject } from '@angular/core/testing';

import { ActivityhistoryUserService } from './activityhistory-user.service';

describe('ActivityhistoryUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityhistoryUserService]
    });
  });

  it('should be created', inject([ActivityhistoryUserService], (service: ActivityhistoryUserService) => {
    expect(service).toBeTruthy();
  }));
});
