import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityhistoryUserComponent } from './activityhistory-user.component';

describe('ActivityhistoryUserComponent', () => {
  let component: ActivityhistoryUserComponent;
  let fixture: ComponentFixture<ActivityhistoryUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityhistoryUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityhistoryUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
