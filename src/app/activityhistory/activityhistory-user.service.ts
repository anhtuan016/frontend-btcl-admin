import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivityhistoryUserService {
  url = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/historyuser";
  constructor(private httpClient:HttpClient) { }
  getUserHistory(page,limit){
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers : new HttpHeaders({
        'Authentication':token,
        'Content-type':'application/json'
      })
    }
    return this.httpClient.get(this.url+'?page='+page+'&limit='+limit,httpOptions);
  }

  search(keyword,page,limit){
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers : new HttpHeaders({
        'Authentication':token,
        'Content-type':'application/json'
      })
    }
    console.log('URL: '+ this.url+'?k='+keyword+'&page='+page+'&limit='+limit);
    return this.httpClient.get(this.url+'?k='+keyword+'&page='+page+'&limit='+limit,httpOptions);
  }
  filterRange(start,stop,page,limit){
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers : new HttpHeaders({
        'Authentication':token,
        'Content-type':'application/json'
      })
    }
    console.log('URL: '+this.url+'?start='+start+'&stop='+stop+'&page='+page+'&limit='+limit)
    return this.httpClient.get(this.url+'?start='+start+'&stop='+stop+'&page='+page+'&limit='+limit,httpOptions);
  }
  

}
