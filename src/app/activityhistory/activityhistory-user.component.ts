import { Component, OnInit } from '@angular/core';
import { ActivityhistoryUserService } from './activityhistory-user.service';

@Component({
  selector: 'app-activityhistory-user',
  templateUrl: './activityhistory-user.component.html',
  styleUrls: ['./activityhistory-user.component.css']
})
export class ActivityhistoryUserComponent implements OnInit {
  limit: number = 10;
  p: number;
  list: any[];
  total: number;
  keyword = null;
  start: any = {};
  stop: any = {};
  start_millisec: number = 0;
  stop_millisec: number = 0;
  flag_filter: boolean; //false-filter by email, true-filter by time.
  constructor(private service:ActivityhistoryUserService) { }

  ngOnInit() {
    this.getPage(1);
  }
  getPage(page) {
    if ((this.keyword === null || this.keyword === undefined || this.keyword === '') && this.start_millisec == 0 && this.stop_millisec == 0) {
      this.service.getUserHistory(page, this.limit).subscribe(resp => {
        console.log(JSON.stringify(resp));
        this.p = resp['page'];
        this.total = resp['totalItem'];
        this.list = resp['data'];
        return;
      }, err => {
        console.log(err.error.detail);
        return;
      });
    }
    if (!this.flag_filter) {
      this.service.search(this.keyword, page, this.limit).subscribe(resp => {
        console.log(JSON.stringify(resp));
        this.list = resp['data'];
        this.p = resp['page'];
        this.total = resp['totalItem'];
      }, err => {
        console.log(err.error.detail);
      })
    }
    if (this.flag_filter) {
      this.service.filterRange(this.start_millisec, this.stop_millisec, page, this.limit).subscribe(resp => {
        this.p = resp['page'];
        this.total = resp['totalItem'];
        this.list = resp['data'];
      }, err => {
        console.log(err.error.detail);
      });
    }

  }
  search(keyword) {
    this.flag_filter = false;
    this.keyword = keyword;
    this.getPage(1);
  }
  filterRange() {
    console.log('start: ' + JSON.stringify(this.start));
    console.log('stop: ' + JSON.stringify(this.stop));
    this.start_millisec = new Date(this.start.year, this.start.month - 1, this.start.day).getTime();
    this.stop_millisec = new Date(this.stop.year, this.stop.month - 1, this.stop.day).getTime();
    if(isNaN(this.start_millisec)) this.start_millisec=0;
    if(isNaN(this.stop_millisec)) this.stop_millisec=0;
    console.log('start_milli: ' + this.start_millisec);
    console.log('stop_millisec: ' + this.stop_millisec);
    this.keyword = null;
    this.flag_filter = true;
    this.getPage(1);

  }
  refreshFilter(){
    this.start={};
    this.stop={};
    this.start_millisec=0;
    this.stop_millisec=0;
    this.keyword=null;
    this.getPage(1);
  }

}
