import { TestBed, inject } from '@angular/core/testing';

import { ActivityhistoryService } from './activityhistory-admin.service';

describe('ActivityhistoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ActivityhistoryService]
    });
  });

  it('should be created', inject([ActivityhistoryService], (service: ActivityhistoryService) => {
    expect(service).toBeTruthy();
  }));
});
