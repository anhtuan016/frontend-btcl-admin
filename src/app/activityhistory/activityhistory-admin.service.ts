import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ActivityhistoryService {
  url_admin = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/historyadmin";

  constructor(private httpClient:HttpClient) { }
  getAdminHistory(page,limit){
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers : new HttpHeaders({
        'Authentication':token,
        'Content-type':'application/json'
      })
    }
    return this.httpClient.get(this.url_admin+'?page='+page+'&limit='+limit,httpOptions);
  }
  search(keyword,page,limit){
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers : new HttpHeaders({
        'Authentication':token,
        'Content-type':'application/json'
      })
    }
    console.log('URL: '+ this.url_admin+'?k='+keyword+'&page='+page+'&limit='+limit);
    return this.httpClient.get(this.url_admin+'?k='+keyword+'&page='+page+'&limit='+limit,httpOptions);
  }
  filterRange(start,stop,page,limit){
    var token = window.localStorage.getItem('token');
    var httpOptions = {
      headers : new HttpHeaders({
        'Authentication':token,
        'Content-type':'application/json'
      })
    }
    console.log('URL: '+this.url_admin+'?start='+start+'&stop='+stop+'&page='+page+'&limit='+limit)
    return this.httpClient.get(this.url_admin+'?start='+start+'&stop='+stop+'&page='+page+'&limit='+limit,httpOptions);
  }


}
