import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AppService {

  constructor(private http: HttpClient) {
  }
  login(jsondata) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/json'
      })
    };
    const url = "https://5-dot-admin-module-dot-bittrexcln.appspot.com/v1/api/admin/login";
    console.log(jsondata);

    return this.http.post(url, jsondata);
  }
  get_access_token_analytics(jwt) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-type': 'application/x-www-form-urlencoded'
      })
    };
    const parameters = "grant_type=" + encodeURIComponent("urn:ietf:params:oauth:grant-type:jwt-bearer") + "&assertion=" + encodeURIComponent(jwt)
    return this.http.post('https://www.googleapis.com/oauth2/v4/token', parameters, httpOptions);
  }
  test_exprire_token(access_token){
    return this.http.get("https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=" + access_token);
  }
}

