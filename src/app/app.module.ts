import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { AppRoutingModule } from './/app-routing.module';
import { ManageuserComponent } from './manageuser/manageuser.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
import { ManageorderComponent } from './manageorder/manageorder.component';
import { BanneduserComponent } from './manageuser/banneduser.component';
import {NgSelectModule} from '@ng-select/ng-select';
import { ManagetransactionComponent } from './managetransaction/managetransaction.component';
import { ActivityhistoryComponent } from './activityhistory/activityhistory-admin.component';
import { ActivityhistoryUserComponent } from './activityhistory/activityhistory-user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CurrencyStatisticComponent } from './dashboard/currency-statistic/currency-statistic.component';
import { ChartModule } from 'primeng/chart';
import {CookieService} from 'ngx-cookie-service';
import { AppService } from './app.service';



@NgModule({
  declarations: [
    AppComponent,
    ManageuserComponent,
    ManageorderComponent,
    BanneduserComponent,
    ManagetransactionComponent,   
    ActivityhistoryComponent,
    ActivityhistoryUserComponent,
    DashboardComponent,
    CurrencyStatisticComponent
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot(),
    NgxPaginationModule,
    NgSelectModule,
    ChartModule
  ],
  providers: [AppService ,CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
